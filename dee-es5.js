
/* global Event fn */

function Ee() {
    if (!(this instanceof Ee)) {
        return new Ee();
    }
    this.event_map = new Map();
    this.once_map = new Map();
    this.one_map = new Map();
    this.all_map = new Map();
}

Ee.prototype = {
    /**
     * Subscript event handler for specified event(s). Dupliate subscript is not allowed.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Ee
     */
    sub: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (!hds) {
                hds = new Set();
                this.event_map.set(eventtype, hds);
            }
            hds.add(handler);
        });
        
        return this;
    }
    ,
    unsub: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (hds && hds.has(handler)) {
                hds.delete(handler);
            }
        });
        
        return this;
    }
    ,
    once: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype =>
            this.once_map.set(eventtype, handler)
        );

        return this;
    }
    ,
    /**
     * When any one of the event-types occurred, the handler will be called and then removed.
     * It means the listening will be stopped and even when another event in the event-types
     * occurred, nothing happens.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    one: function(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }

        this.one_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }
    ,
    /**
     * After all event-types have occurred, the handler will be called and then removed.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    all: function(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }
        this.all_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }
    ,
    pub: function (eventtype, arg, thisarg=null, run=bee_TimeToRun.immediate) {
        let ctx = thisarg ? thisarg : this;

        // call handlers in one_map
        this.one_map.forEach(function(keyvalue) {
            var hd = keyvalue[0];
            var ets = keyvalue[1];
            if(ets.has(eventtype)) {
                this._handle(hd, arg, ctx, run);
                this.one_map.delete(hd);
            }
        }, this);
        
        this.all_map.forEach(function(keyvalue) {
            var hd = keyvalue[0];
            var ets = keyvalue[1];
            if(ets.delete(eventtype) && 0 == ets.size) {
                this._handle(hd, arg, ctx, run);
                this.all_map.delete(hd);
            }
        });

        // call handlers in once_map
        let hds = this.once_map.get(eventtype);
        if (hds) {
            hds.forEach(function(hd) {
                this._handle(hd, arg, ctx, run);
            }, this);
            hds.delete(eventtype);
        }

        // call handlers in event_map
        hds = this.event_map.get(eventtype);
        if (hds) {
            hds.forEach(function(hd) {
                this._handle(hd, arg, ctx, run);
            }, this);
        }
        
        return this;
    }
    ,
    _handle: function(handler, arg, thisarg, timetorun) {
        let hd = handler;
        if (thisarg) {
            hd = handler.bind(thisarg);
        }
    
        switch(timetorun) {
            case bee_TimeToRun.nexttick:
                Promise.resolve().then(() => hd(arg));
                break;
            case bee_TimeToRun.nextturn:
                setTimeout(() => hd(arg), 0);
                break;
            default:
                hd(arg);
                break;
        }
        
        return this;
    }
};

var KEY_EVT_PROXY = "event_proxy";
var KEY_EVT_HANDLER = "event_handler";
var KEY_EVT_LISTENER = "event_listener";

/**
 * 
 * Listener will be called in the order it's registered.
 * 
 * @class BeeE
 */
function BeeE(elem) {
    if (!(this instanceof BeeE)) {
        return new BeeE(elem);
    }
    this._element = elem;
    this._attached = true;
    this.eventMap = new Map();
}

BeeE.prototype = {
    element : function(v) {
        if (arguments.length > 0) {
            this._element = v;
            return this;
        }
        return this._element;
    }
    ,
    attached : function(v) {
        if (arguments.length > 0) {
            this._attached = v;
            return this;
        }
        return this._attached;
    }
    ,
    addEventListener : function(eventtype, handler, useCapture=false, thisarg=null) {
        if (!this.eventMap.has(eventtype)) {
            this.eventMap.set(eventtype, new Map());
        }

        if (this.eventMap.get(eventtype).has(handler)) {
            return;         // duplicate
        }

        var listener = this.makeListener(eventtype, handler, thisarg);
        // duplicate would be overwritten
        this.eventMap.get(eventtype).set(handler, {
            KEY_EVT_LISTENER: listener,
        });

        this._element.addEventListener(eventtype, listener, useCapture);
        
        return this;
    }
    ,
    removeEventListener : function(eventtype, handler) {
        if (this.eventMap.has(eventtype)) {
            if (handler) {
                this._element.removeEventListener(eventtype, this.eventMap.get(eventtype).get(handler).KEY_EVT_PROXY);
                this.eventMap.get(eventtype).delete(handler);
            } else {
                this.eventMap.get(eventtype).forEach((obj, listener) => {
                    this._element.removeEventListener(eventtype, obj.KEY_EVT_PROXY);
                    this.eventMap.get(eventtype).delete(listener);
                });
            }
            if (this.eventMap.get(eventtype).size === 0) {
                this.eventMap.delete(eventtype);
            }
        }
        return this;
    }
    ,
    removeAllListeners : function() {
        this.eventMap.forEach((obj, eventtype) => {
            obj.forEach((o, listener) => {
                this._element.removeEventListener(eventtype, o.KEY_EVT_PROXY);
                obj.delete(listener);
            });
        });
        return this;
    }
    ,
    dispatchEvent : function(eventtype, data=undefined) {
        var event = new Event(eventtype, data);
        this._element.dispatchEvent(event);
        return this;
    }
    ,
    getListener : function(eventtype, handler) {
        return !this.eventMap.has(eventtype)
             ? null
             : !this.eventMap.get(eventtype).has(handler)
             ? this.eventMap.get(eventtype).get(handler).KEY_EVT_LISTENER
             : null
             ;
    }
    ,
    makeListener : function(eventtype, handler, thisarg) {
        return function(event) {
            if (!this.attached()) {
                return;
            }
            
            this.eventMap.get(eventtype).set(KEY_EVT_HANDLER, handler);
            var proxy = this.eventMap.get(eventtype).get(KEY_EVT_PROXY);
            if (proxy && typeof proxy === "function") {
                if (thisarg && typeof thisarg === "object") {
                    proxy = proxy.bind(thisarg);
                }
                proxy(handler, event);
            } else {
                if (thisarg && typeof thisarg === "object") {
                    handler = handler.bind(thisarg);
                }
                handler(event);
            }
            // TIMEs? async?
            // web worker?
        }.bind(this);
    }
    ,
    proxy : function(eventtypes, func) {
        var evtList = fn.stringToArray(eventtypes);
        evtList.forEach(eventtype => {
            var evt = this.eventMap.get(eventtype);
            if (evt) {
                evt.set(KEY_EVT_PROXY, func);
            }
        });
        return this;
    }
};

function Bee(elements) {
    /**
     * Creates an instance of Bee.
     * @param {any} [elements=null] A string or string array of selector(s) or an elements array.
     * @memberof Bee
     */
    if (!elements) elements = null;
    this.elementMap = new Map();        // containing element-event map
    this.bind(elements);
}

Bee.prototype = {
    bind : function(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (!this.elementMap.has(element)) {
                    this.elementMap.set(element, null);
                }
            });
        }
        return this;
    }
    ,
    unbind : function(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (this.elementMap.has(element)) {
                    var beee = this.elementMap.get(element);
                    beee.removeAllListeners();
                    this.elementMap.delete(beee);
                }
            });
        }
        return this;
    }
    ,
    // TODO: add element as the first argument?
    /**
     * Add event handler to element(s)
     * 
     * @param {*} eventtypes                Event type names. It can be an array or a string.
     *                                      In case string, space character is used to delimit the types.
     * @param {Function} handler            Event handler.
     * @param {Boolean} [useCapture=null]   Indicating using capture or bubbling when event fired.
     * @param {*} [thisarg=null]            Binding "this" object to the event handler. 
     *                                      If null, the "element" should be "this" by default.
     * @memberof Bee
     */
    on : function(eventtypes, handler, useCapture=false, thisarg=null) {
        var evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (!beee) {
                    this.elementMap.set(element, new BeeE(element));
                }
                this.elementMap.get(element).addEventListener(eventtype, handler, useCapture, thisarg);
            });
        });
        return this;
    }
    ,
    /**
     * Remove event handler from element(s) 
     *
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Bee
     */
    off : function(eventtypes, handler) {
        var evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (beee) {
                    beee.removeEventListener(eventtype, handler);
                    if (beee.size === 0) {
                        this.elementMap.delete(element);
                    }
                }
            });
        });
        return this;
    }
    ,
    /**
     * Require proxy to attach handler so it can react to events.
     * 
     * @param {any} [elements=null] Elements to attach. Refer to on() for detail information.
     * @memberof Bee
     */
    attach : function() {
        this.elementMap.forEach((beee) => {
            beee.attached(true);
        });
        return this;
    }
    ,
    /**
     * Require proxy to detach handler from responsing events. So even when event occurred,
     * the handler would not react to it.
     * 
     * @param {any} [elements=null] Elements to detach. Refer to on() for detail information.
     * @memberof Bee
     */
    detach : function() {
        this.elementMap.forEach((beee) => {
            beee.attached(false);
        });
        return this;
    }
    ,
    /***
     * @param {any} func Proxy of the registered event handler.
     *                   The contract of this function is func(handler, event).
     *                   The proxy can be removed by setting argument func to null or undefined.
     * */
    proxy : function(eventtypes, func) {
        if (func && typeof func !== "function") {
            return;
        }
        this.elementMap.forEach((beee) => {
            beee.proxy(eventtypes, func);
        });
        return this;
    }
    ,
    /**
     * Fire an event to the element(s)
     * 
     * @param {any} eventtype 
     * @param {any} data 
     * @param {any} [elements=null] Elements to which dispatch event. Refer to on() for detail information.
     * @memberof Bee
     */
    dispatch : function(eventtype, data) {
        this.elementMap.forEach((beee) => {
            beee.dispatchEvent(eventtype, data);
        });
        return this;
    }
};

var bee_TimeToRun = Object.freeze({
    immediate: "immediate",
    nexttick : "nexttick",
    nextturn : "nextturn"
});

/* global fn Bee */

function $(selector, context, firstonly) {
    return new DOM(selector, context, firstonly);
}

$.fn = fn;

function DOM(selector, context, firstonly) {
    if (!selector) {
        // TODO: return closure:
        //       1. when no selector or invalid selector, closure contains methods 
        //          which have nothing to do with elements (such as util), which is a singletone object
        //       2. when valid selector presents, closure return this instance, excluding the "_x" or "__x" objects
        //          eg. return {html: html.bind(this)}
        return this;
    }
    
    var ret = this,
        contextElement;
    
    if (!!context) {
        contextElement = context;
    } else {
        contextElement = document;
    }
    
    this.length = 0;

    if (typeof selector == "string") {
        var match = null;
        if (null != (match = fn.parseHtml(selector))) {
            if (Array.isArray(match)) {
                throw "Only a html snippet with a root element is supported.";
            }
            var ele = document.createElement(match["tag"]);
            
            match["attributes"].forEach(a => {
                ele.setAttribute(a.attrName, a.attrValue);
            });
            
            ele.innerHTML = match["innerHTML"];
            this.push(ele);
        } else if (contextElement === document || this.isElement(contextElement)) {
            this.merge(this.query(contextElement, selector, firstonly));
        } else if (contextElement instanceof DOM) {
            contextElement.forEach((index, element) => {
                this.merge(this.query(element, selector, firstonly));
            });
        }
    } else if (Array.isArray(selector)) {
        this.merge(selector);
    } else if (this.isElement(selector)) {
        this.push(selector);
    } else if (selector instanceof DOM) {
        ret = selector;
    }
    
    this.$$bee = new Bee(this);
    
    return ret;
}

DOM.prototype = {
    on : function(eventtypes, handler, useCapture=false, thisarg=null) {
        this.$$bee.on(eventtypes, handler, useCapture, thisarg);
        return this;
    }
    ,
    off : function(eventtypes, handler) {
        this.$$bee.off(eventtypes, handler);
        return this;
    }
    ,
    attach : function() {
        this.$$bee.attach();
        return this;
    }
    ,
    detach : function() {
        this.$$bee.detach();
        return this;
    }
    ,
    eventProxy : function(eventtypes, func) {
        this.$$bee.proxy(eventtypes, func);
        return this;
    }
    ,
    push : function() {
        Array.prototype.push.apply(this, Array.from(arguments));
        return this;
    }
    ,
    slice : function() {
        Array.prototype.slice.apply(this, Array.from(arguments));
    }
    ,
    splice : function() {
        Array.prototype.splice.apply(this, Array.from(arguments));
    }
    ,
    each : function(callback) {
        if (callback)
        for(var i = 0; i < this.length; i++) {
            if (callback(i, this[i])) {
                break;
            }
        }
        return this;
    }
    ,
    html : function(value) {
        var ret = this;
        if (this.isHtml(value)) {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    element.innerHTML = value;
                }
            });
        } else {
            ret = "";
            this.each((index, element) => {
                if (this.isElement(element)) {
                    ret += element.innerHTML;
                }
            });
        }
        return ret;
    }
    ,
    ready : function(domcontentready, callback) {
        var _contentonly = domcontentready,
            _callback = callback;
            
        if (undefined == callback) {
            _contentonly = true;
            _callback = domcontentready;
        }
        
        if (_contentonly) {
            document.addEventListener("DOMContentLoaded", callback);
        } else {
            window.addEventListener("load", callback);
        }
    }
    ,
    hasClass : function(className) {
        var ret = false,
            regClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
        
        this.each((index, element) => {
            if (this.isElement(element) && regClass.test(element.className)) {
                ret = true;     // hasClass = true
                return false;   // break from each
            }
        });
        
        return ret;
    }
    ,
    addClass : function(value) {
        if (typeof value === "function") {
            /* A function returning one or more space-separated class names to be added to the existing class name(s).
             * Within the function, this refers to the current element in the set
             */
            this.each((index, element) => {
                var rst = value.call(element, index, element.className);
                if (!!rst) {
                    fn.addClassToElement(element, rst);
                }
            });
        } else {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    fn.addClassToElement(element, value);
                }
            });
        }
        
        return this;
    }
    ,
    removeClass : function(value) {
        if (typeof value === "function") {
            /* A function returning one or more space-separated class names to be removed to the existing class name(s).
             * Within the function, this refers to the current element in the set
             */
            this.each((index, element) => {
                var rst = value.call(element, index, element);
                if (!!rst) {
                    element.className = fn.removeClassFromElement(element, rst);
                }
            });
        } else {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    fn.removeClassFromElement(element, value);
                }
            });
        }
        
        return this;
    }
    ,
    /** Better performance than removeClass->addClass  */
    replaceClass : function(oldclass, newclass) {
        var classes;
        if (typeof oldclass === "string") {
            if (undefined != newclass) {
                classes = {};
                classes.oldclass = newclass;
            }
        } else if (typeof oldclass === "object") {
            classes = oldclass;
        }
        
        if (undefined == classes) {
            throw new Error("replaceClass(): Invalid paramter.");
        }
        
        this.each(function(index, element) {
            var cls = element.className;
            cls.split(" ").forEach(function(cur, idx, arr) {
                if (classes.hasOwnProperty(cur)) {
                    cls = cls.replace(new RegExp("(^|\\s)" + cur + "(\\s|$)"), " " + classes[cur] + " ");
                }
            });
            element.className = cls.replace(/ {2,}/g, " ");
        });
        
        return this;
    }
    ,
    attr : function(attrName, value) {
        if (undefined == value) {
            return this[0].getAttribute(attrName);
        } else {
            this.each(function(index, element) {
                if (this.isElement(element)) {
                    element.setAttribute(attrName, value);
                }
            }.bind(this));
        }
        
        return this;
    }
    ,
    hasAttribute : function(attr) {
        return this[0].hasAttribute(attr);
    }
    ,
    // TODO: (property [, value])
    css : function(prop, value) {
        var ret = this;
        if (undefined != value) {
            this.each(function(index, element) {
                element.style[prop] = value;
            });
        } else {
            if ("string" === typeof prop) {
                ret = this[0].style[prop];
            } else if (Array.isArray(prop)) {
                if (0 < prop.length) {
                    ret = {};
                    prop.forEach(function(p) {
                        ret[p] = this[0].style[p];
                    }.bind(this));
                }
            } else if ("object" === typeof prop ) {
                var cssText = "";
                for (var key in prop) {
                    if (prop.hasOwnProperty(key)) {
                        cssText += key + ":" + prop[key] + ";";
                    }
                }
                this.cssText(cssText);
            }
        }
        return ret;
    }
    ,
    cssText : function(csstext) {
        var ret = this[0].cssText;
        if (!!csstext && typeof(csstext) == "string") {
            var csst = csstext.trim();
            csst = csst.replace(/(\r\n|\n|\r)/gm,"");
            if (!csst.endsWith(";")) {
                csst += ";"; 
            }
            
            var pattern = /\s*([^;|^:]+):[^;]*;/g;
            var props = [];
            var match;
            while (null != (match = pattern.exec(csst))) {
                props.push(match[1]);
            }
            
            this.each(function(index, element) {
                var oriStyle = element.style.cssText;
                if (!oriStyle.endsWith(";")) {
                    oriStyle = oriStyle + ";";
                }
                
                props.forEach(function(prop) {
                   oriStyle = oriStyle.replace(new RegExp("\\s*" + prop + "\\s*:[^;]*;\\s*", "gi"), "");
                });
                
                var delim = oriStyle.endsWith(";") ? "" : ";";
                element.style.cssText = oriStyle + delim + csst;
            });
            
            ret = this;
        }
        return ret;
    }
    ,
    val : function(value) {
        return this.attr("value", value);
    }
    ,
    text : function(text) {
        var ret = this;
        
        if (undefined != text) {
            this.each(function(index, element) {
                element.innerHTML = text;
            });
        } else {
            ret = "";
            this.each(function(index, element) {
                ret += element.innerHTML + " ";
            });
        }
        
        return ret;
    }
    ,
    viewportSize : function() {
        var h = window.innerHeight 
              || document.documentElement.clientWidth 
              || document.getElementsByTagName("body")[0].clientWidth
              ,
            w = window.innerWidth
              || document.documentElement.clientHeight
              || document.getElementsByTagName("body")[0].clientHeight
              ;
        
        return {width: w, height: h};
    }
    ,
    // TODO: make getComputedStyle returned value a sington one.
    // in regard to the returned value of getComputedStyle, it reads
    // "returned style is a live CSSStyleDeclaration object, which updates itself automatically 
    //  when the element's style is changed."
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/getComputedStyle
    
    /**
     * Set or get the element width value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    width : function(w) {
        var ret = this;
        
        if (undefined != w) {
            if (typeof(w) === "number") {
                w += "px";
            }
            this.each(function(index, element) {
                element.style.width = w;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null).width);
        }
        
        return ret;
    }
    ,
    /**
     * Set or get the element width value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    height : function(h) {
        var ret = this;
        if (undefined != h) {
            if (typeof(h) === "number") {
                h += "px";
            }
            this.each(function(index, element) {
                element.style.height = h;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null).height);
        }
        
        return ret;
    }
    ,
    /**
     * Set or get the element top value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    top : function(t) {
        var ret = this;
        
        if (undefined != t) {
            if (typeof(t) === "number") {
                t += "px";
            }
            this.each(function(index, element) {
                element.style.top = t;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null)).top;
        }
        
        return ret;
    }
    ,
    /**
     * Set or get the element left value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    left : function(l) {
        var ret = this;
        
        if (undefined != l) {
            if (typeof(l) === "number") {
                l += "px";
            }
            this.each(function(index, element) {
                element.style.left = l;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null)).left;
        }
        
        return ret;
    }
    ,
    show : function() {
        this.each(function(index, element){
            element.style.display = "";
        });
        
        return this;
    }
    ,
    hide : function() {
        this.each(function(index, element){
            element.style.display = "none";
        });
        
        return this;
    }
    ,
    visible : function(bvisible) {
        this.each(function(index, element){
            element.style.visibility = bvisible ? "visible" : "hidden";
        });
        
        return this;
    }
    ,
    onOrientationChanged : function(listener) {
        var mqlPortrait = !!window.matchMedia ? window.matchMedia("(orientation: portrait)") : null;
        if (!!mqlPortrait) {
            mqlPortrait.addListener((mql) => {
                listener(mqlPortrait.matches);
            });
        } else {
            window.addEventListener("resize", function() {
                listener(window.innerHeight > window.innerWidth); // windows.innerHeight / innerWidth would cause reflow
            });
        }
        return this;
    }
    ,
    append : function(content) {
        if (this.isElement(content)) {
            this.each((index, element) => {
                element.appendChild(content);
            });
        } else if (this.isHtml(content)) {
            this.append(new DOM(content)[0]);
        } else if (content instanceof DOM) {
            this.append(content[0]);
        }

        return this;
    }
    ,
    appendTo : function(owner) {
        if (undefined != owner) {
            this.each((index, element) => {
                new DOM(owner).append(element);
            });
        }
        
        return this;
    }
    ,
    remove : function(child) {
        if (this.isElement(child)) {
            this.each((index, element) => {
                element.removeChild(child);
            });
        } else if (child instanceof DOM) {
            this.each((index, element) => {
                child.forEach(function(index, child) {
                    element.removeChild(child);
                });
            });
        } else if (this.isString(child)) {
            this.remove(new DOM(child)).bind(this);
        } else if (null == child || undefined == child) {
            this.each((index, element) => {
                for (var i = 0; i < element.children.length; i++) {
                    element.removeChild(element.children[i]);
                }
            });
        }
        return this;
    }
}

fn.addClassToElement = function(element, classValue) {
    var cls = element.className.trim(),
        arr = fn.isArrayOrLike(classValue) 
            ? classValue
            : typeof classValue === "string"
            ? classValue.split(" ")
            : []
            ;
    
    for(var i=0; i<arr.length; i++) {
        var cur = arr[i];
        if (!cls) {
            cls = cur;
        } else if(0 > cls.split(" ").indexOf(cur)) { // case sensitive
            cls += " " + cur;
        }
        cls = cls.replace(/ {2,}/g, " ");
    }
    
    element.className = cls;
    
    return this;
};

fn.removeClassFromElement = function(element, classValue) {
    var cls = element.className,
        arr = fn.isArrayOrLike(classValue) 
            ? classValue
            : typeof classValue === "string"
            ? classValue.split(" ")
            : []
            ;
    
    if (!!cls) {
        for(var i=0; i<arr.length; i++) {
            cls = cls.replace(new RegExp("(^|\\s)" + arr[i] + "(\\s|$)", "g") ," ");
        }
        cls.replace(/ {2,}/g, " ");
    }
    element.className = cls;
    
    return this;
};

/**
 * description  parse "tag" "attributes" "innerHtml" from html (like) string.
 * 
 * TODO: the behavor should be like as follows. At the being, only one root element html snippet is supported.
 * If the string has only one "root" element, the return value is a object has tag/attributes/innerHtml key-value pair.
 * If the string has multi cousin element without "root" element, then the return value is an array contains key-value paires.
 *
 */
fn.parseHtml = function(html) {
    // TODO: /\s*((?:regex1)|(?:regex2))\s*/gm, (no ^ $), and parse to multiple cousin elements
    var regex1 = /^\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>]*)\s*>(.*)<\/([A-Za-z][A-Za-z0-9]+)\s*>\s*$/,
        regex2 = /^\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>\/]*)\s*\/{0,1}>\s*$/,
        result = null,
        match = null;
        
    if (typeof html == "string") {
        var arrAttr =[];
        if (null != (match = regex1.exec(html))) {
            if (match[1] === match[4]) {
                arrAttr = this.parseAttrStr(match[2]);
                result = {
                    tag: match[1],
                    attributes: arrAttr,
                    innerHTML: match[3]
                };
            } else {
                throw "The open tag does not match close tag";
            }
        } else if (null != (match = regex2.exec(html))) {
            arrAttr = this.parseAttrStr(match[2]);
            result = {
                tag: match[1],
                attributes: arrAttr,
                innerHTML: ""
            };
        }
    }
    
    return result;
};

fn.parseAttrStr = function(attrStr) {
    var arrAttr = [];
    if (!!attrStr && !/^\s+$/.test(attrStr)) {
        var a = attrStr.match(/(?:[^\s"']+|["'][^"']*["'])+/g) ;
        a.forEach(function(s) {
            if (!/^\s+$/.test(s)) {
                var kv = s.split("=");
                if (2 < kv.length) {
                    throw "Invalid html attribute format!";
                }
                var key = kv[0].trim();
                var val = null;
                if (2 == kv.length) {
                    val = kv[1].trim().replace(/^["']/, '').replace(/["']$/, '');
                }
                arrAttr.push({attrName: key, attrValue: val});
            }
        });
    }
    return arrAttr;
};

fn.query = function(element, selector, firstonly) {
    selector = selector.trim();
    var rst = [];
    
    if (firstonly) {
        rst.push(element.querySelector(selector));
    } else {
        rst = element.querySelectorAll(selector);
    }
    
    return rst;
};

/***
 * $.apply is different from $.fn.apply.
 * $.apply will apply the object to DOM.prototype,
 * while $.fn.apply can not apply to DOM instance or other specified object.
 * */
$.apply = function(obj) {
    fn.extend(true, DOM.prototype, obj);
    return $;
};

fn.extend(false, DOM.prototype, fn);

// export { fn };

/* global Element, HTMLElement*/

var fn = {
    isObject : function(obj) {
        return obj !== null && typeof obj === "object";
    }
    ,
    isCallable : function (fn) {
      return typeof fn === 'function' || Object.prototype.toString.call(fn) === '[object Function]';
    }
    ,
    isElement : function(obj) {
        return !obj
                ? false
                : (Element && typeof Element === "object")
                ? obj instanceof Element
                : (HTMLElement && typeof HTMLElement === "object")
                ? obj instanceof HTMLElement
                : (typeof obj === "object" && obj.nodeType === 1 && typeof obj.nodeName === "string")
                ;
    }
    ,
    /* global Node */
    isNode: function(obj) {
        return typeof Node === "object"
                ? obj instanceof Node
                : obj && typeof obj === "object" && typeof obj.nodeType === "number" && typeof obj.nodeName === "string"
                ;
    }
    ,
    isHtml: function(value) {
        var regexx = /^\s*<.*>\s*$/,
            regex1 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>]*)\s*>(.*)<\/([A-Za-z][A-Za-z0-9]+)\s*>\s*/g,
            regex2 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>\/]*)\s*\/{0,1}>\s*/g
            ;
        return regexx.test(value) && (regex1.test(value) | regex2.test(value));
    }
    ,
    isString : function(s) {
        return typeof s === "string" || s instanceof String;
    }
    ,
    isArrayOrLike : function(a) {
        return !a
                ? false
                : a instanceof Array
                ? true
                : typeof(a) == "object" && a.hasOwnProperty("length") && a.length >= 0 && a.length < Number.MAX_SAFE_INTEGER
                ;
    }
    ,
    /**
     * @description The objects which can apply forEach() to, but not enumerable (which means an object here.)
     *              Check if the target object can be used in for() loop.
     */
    isIterable : function(obj) {
        var rst = false,
            strType = Object.prototype.toString.call( obj );
        
        // TODO: add set, map, iterator?
        if (fn.isArrayOrLike(obj)
        ||  /^\[object (HTMLCollection|NodeList)\]$/.test(strType))
        {
            rst = true;
        }
        
        return rst;
    }
    ,
    isUndefine : function(obj) {
        return typeof obj === 'undefined';
    }
    ,
    isDate : function(dat) {
        return Object.prototype.toString.call(dat) === '[object Date]';
    }
    ,
    isRegexp : function(reg) {
        return Object.prototype.toString.call(reg) === '[object RegExp]';
    }
    ,
    /***
     * Parse words in a string delimited by space into word arrays.
     * */
    stringToArray : function(s) {
        var arr = s;
        if (fn.isString(s)) {
            arr = s.split(" ");
            arr = arr.filter(a => {
                    a = a.trim();
                    return !!a;
                });
        }
        return arr;
    }
    ,
    flattenArrayOrLike : function(arr) {
        var a = [];
        if (fn.isArrayOrLike(arr))
        for (var i = 0; i < arr.length; i++) {
            if (fn.isArrayOrLike(arr[i])) {
                a = a.concat(a, arr[i]);
            } else {
                a.push(arr[i]);
            }
        }
        return a;
    }
    ,
    flattenElements : function(elements, arr) {
        if (!window || !window.document) {
            throw new Error("Illegal operation. The context seems not a browser window object.");
        }

        if (arr && !fn.isArrayOrLike(arr)) {
            throw new Error("Invalid argment. The second argument should be an array.");
        }
        
        var elelist = arr || [];

        if (fn.isString(elements)) {
            elelist = window.document.body.querySelectorAll(elements);
        } else if (fn.isArrayOrLike(elements)) {
            fn.flattenArrayOrLike(elements).forEach( el => {
                if (fn.isString(el)) {
                    elelist = elelist.concat(elelist, window.document.body.querySelectorAll(el));
                } else if (fn.isElement(el)) {
                    elelist.push(el);
                } else {
                    throw new Error("Invalid argument. Selectors or HtmlElement objects are expected.");
                }
            });
        } else {
            throw new Error("Invalid argument. A string or an array (or -like) is expected");
        }

        return elelist;
    }
    ,
    /**
     * NOTE: the arguments other than plain object would be wrapped into an object.
     */
    shallowClone : function(obj) {
        var rst = obj;
        if (null === obj || undefined === obj) {
            rst = this;
        }
        if (typeof obj === "function") {
            rst = obj;
        } else if (obj !== null && obj !== undefined && obj instanceof Object) {
            for (var nextKey in obj) {
                if (obj.hasOwnProperty(nextKey)) {
                    rst[nextKey] = obj[nextKey];
                }
            }
        }
        return rst;
    }
    ,
    /**
     * description
     * refer to [https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm]
     */
    deepClone : function(obj) {
        var rst = obj,
            Constructor;

        if (null === obj && undefined === obj) {
            obj = this;
        }
        
        if (obj instanceof Object) {
            var copy = true;
            Constructor = obj.constructor;
            switch (Constructor) {
                /* global CanvasRenderingContext2D */
                case CanvasRenderingContext2D:
                    copy = false;   // for those special objects, do not need copy them
                case Function:
                    rst = obj;
                    break;
                case RegExp:
                case String:
                case Number:
                    rst = new Constructor(obj);
                    break;
                case Date:
                    rst = new Constructor(obj.getTime());
                    break;
                default:
                    rst = new Constructor();
                    break;
            }
            
            if (copy)
            for (var prop in obj) {
                rst[prop] = fn.deepClone(obj[prop]);
            }
        }
        
        return rst;
    }
    ,
    /**
     * @description perform shallow or deep cloning.
     * @param {boolean} [ifDeep=false]
     * @param {object} obj
     */
    clone : function(deep, obj) {
        var idx = 0,
            _deep = false,
            _obj;
        
        if (typeof arguments[idx] === "boolean") {
            _deep = arguments[idx++];
        }
        
        _obj = arguments[idx];
        if (undefined === _obj || null === obj) {
            _obj = this;
        }
        
        if (!_deep) {
            return fn.shallowClone(_obj);
        } else {
            return fn.deepClone(_obj);
        }
    }
    ,
    /**
     * 
     */
    extend : function(deep, target, obj) {
        var idx = 0,
            _deep = false,
            _target = this,
            _obj = obj;

        if (arguments.length <= 0) {
            return _target;
        }
        
        // deep
        if (typeof arguments[idx] === "boolean") {
            _deep = arguments[idx++];
        }
        
        // target
        if (idx >= arguments.length || !arguments[idx]) {
            return _target;
        }
        
        // set target
        _target = arguments[idx++];
        
        _obj = arguments[idx];
        
        // extends all objects to targets.
        for (; idx < arguments.length; idx++) {
            var o = _obj;
            if (o == null || o == undefined) continue;

            for (var prop in o) {
                _target[prop] = fn.clone(_deep, o[prop]);
            }
        }

        return _target;
    }
    ,
    apply : function(target, obj) {
        var _target = target,
            _obj = obj;
            
        if (1 == arguments.length) {
            _target = this;
            _obj = target;
        }
        fn.extend(true, _target, _obj);     // should not be affect by other objects who keep the obj.
        return this;
    }
    ,
    /**
     * Create a new function which inherits from parent, and with new properties against parent.
     */
    create : function(parent, newProperties, independant) {
        if (undefined == parent) return null;
        if (!newProperties) return parent;
        
        return function() {
            function T(){}
            if (parent.prototype) {
                T.prototype = fn.clone(independant, parent.prototype);
            }
            if (T.prototype) {
                fn.extend(independant, T.prototype, newProperties);
            }
            var t = new T();
            var p = parent.prototype;
            parent.prototype = t;
            var f = parent.apply(t, arguments);
            parent.prototype = p;
            return f;
        };
    }
    ,
    /** arguments: ([arraylikeobj], callback) */
    forEach : function(obj, callback, isToBreak) {
        var _obj = this,
            _callback,
            _breakSwitch = false;
        
        switch(arguments.length) {
            case 1:
                _callback = arguments[0];
                break;
            case 2:
                if (typeof arguments[0] === "function")
                {
                    _callback = arguments[0];
                    _breakSwitch = arguments[1];
                } else {
                    _obj = arguments[0];
                    _callback = arguments[1];
                }
                break;
            default:
                _obj = arguments[0];
                _callback = arguments[1];
                _breakSwitch = isToBreak;
                break;
        }
        
        if (typeof _obj === "object")
        if( fn.isIterable(_obj) ) {
            for (var i = 0; i < _obj.length; i++) {
                if (_callback.call(_obj[i], i, _obj[i]) === _breakSwitch) {
                    break;
                }
            }
        } else {
            for (var prop in _obj) {
                if (_callback.call(_obj[prop], prop, _obj[prop]) === _breakSwitch) {
                    break;
                }
            }
        }
        
        return obj;
    }
    ,
    /**
     * @description Merge two arrays.
     * TODO: distinct?
     */
    merge : function(arr1, arr2) {
        var
        _arr1 = arr1,
        _arr2 = arr2,
        _start;
        
        if (2 > arguments.length) {
            _arr1 = this;
            _arr2 = arr1;
        }
        
        _start = _arr1.length;

        for(var i = 0; i < _arr2.length; i++) {
            _arr1[i + _start] = _arr2[i];
        }
        
        _arr1.length = _arr1.length + _arr2.length;
        
        return _arr1;
    }
    ,
    format : function(fmt) {
        if (!fmt) {
            return fmt;
        }
        
        var idx,
            match,
            reg = /({\s*(\d+)\s*})/g;
        
        while (null != (match = reg.exec(fmt))) {
            if (!isNaN(idx = Number.parseInt(match[2], 10))) {
                fmt = fmt.replace(match[1], arguments[idx+1]);      // {x} x is 0 based
            } else {
                break;
            }
        }
        return fmt;
    }
    ,
    equals : function(o1, o2) {
        var ret = (o1 === o2);
        if (ret) return true;
        
        if (typeof(o1) === "number" && isNaN(o1) && 
            typeof(o2) === "number" && isNaN(o2)) {
                return true;
            }
            
        if (!o1 && !o2) {
            if (Array.isArray(o1) && Array.isArray(o2) && o1.length == o2.length) {
                ret = true;
                for (var i = 0; i < o1.length; i++) {
                    if (!this.equals(o1[i], o2[i])) {
                        ret = false;
                        break;
                    }
                }
            } else if (this.isObject(o1) && this.isObject(o2)) {
                ret = true;
                for(var p1 in o1) {
                    if (o1.hasOwnProperty(p1) && !this.equals(o1[p1], o2[p1])) {
                        ret = false;
                        break;
                    }
                }
                if (ret) {
                    for(var p2 in o2) {
                        if (o2.hasOwnProperty(p2) && !this.equals(o1[p2], o2[p2])) {
                            ret = false;
                            break;
                        }
                    }
                }
            }
        }
        
        return ret;
    }
};

/** Add methods to String object */
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = [].slice.call(arguments);
        args.unshift(this);
        return fn.format.apply(this, args);
    };
}


/***
 * Polyfill for es6 Map object. Not all methods will be implemented.
 * Just those methods we will use.
 * 
 * Since this polyfill is just for using with es5, iterator would not be honored.
 * */
(function(context) {
    if (!!context.Map) return;
    
    var Map = function(array) {
        if (!(this instanceof Map)) {
            return new Map(array);
        }
        
        var check = true;
        if (!Array.isArray()) {
            check = false;
        }
        array.forEach(function(keyvalue){
            if (!Array.isArray(keyvalue) || keyvalue.length != 2) {
                check = false;
                return;
            }
        }, this);
        if (!check) {
            throw new Error("The argument sent to Map constructor should be a key-value pair array.");
        }
        
        this.__keyvaluepairs__ = array || new Array();
        this.size = 0;
    };
    
    Map.prototype = {
        _idxKey: 0,
        _idxValue: 1,
        /***
         * check if the specified key has been set. 
         * out_obj_for_idx is not presented in the standard IF.
         * It is here to keep the key location, and just for using within the Map object.
         * */
        has: function(key, out_obj_for_idx) {
            var ret = false;
            // since Nan cannot be handled correctly by indexOf(), we iterate via forEach.
            this.__keyvaluepairs__.forEach(function(keyvalue, index){
                if ((typeof(key) === "number" && isNaN(key) && 
                    typeof(keyvalue[this._idxKey]) === "number" && isNaN(keyvalue[this._idxKey]))  ||
                    key === keyvalue[0]) 
                {
                    ret = true;
                    if (!!out_obj_for_idx && (out_obj_for_idx instanceof Object)) {
                        out_obj_for_idx.index = index;
                    }
                    return;
                }
            }, this);
            return ret;
        }
        ,
        set: function(key, value) {
            if (this.has(key)) {
                this.__keyvaluepairs__[key][this._idxValue] = value;
            } else {
                this.__keyvaluepairs__.push([key, value]);
                this.size ++;
            }
            return this;
        }
        ,
        get: function(key) {
            var ret;
            if (this.has(key)) {
                ret = this.__keyvaluepairs__[key];
            }
            return ret;
        }
        ,
        clear: function() {
            this.__keyvaluepairs__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(key) {
            var ret = false,
                idx = {};
            if (this.has(key, idx)) {
                this.__keyvaluepairs__.splice(idx.index, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg){
            var me = this;
            thisarg = thisarg || this;
            this.__keyvaluepairs__.forEach(function(keyvalue) {
                callback(keyvalue[me._idxValue], keyvalue[me._idxKey], me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Map = Map;
})(window || global);

(function(context){
    if (!!context.Set) return;
    
    var Set = function(array) {
        if (!(this instanceof Set)) {
            return new Set(array);
        }
        
        this.size = 0;
        this.__data__ = [];
    };
    
    Set.prototype = {
        has: function(value, out_obj_for_idx) {
            var ret = false;
            if (typeof(value) === "number" && isNaN(value)) {
                this.__data__.forEach(function(val, index) {
                    if (typeof(val) === "number" && isNaN(val)) {
                        ret = true;
                        out_obj_for_idx.idx = index;
                    }
                });
            } else if ((out_obj_for_idx.idx = this.__data__.indexOf(value, 0)) >= 0) {
                ret = true;
            }
            return ret;
        }
        ,
        add: function(value) {
            if (!this.has(value)) {
                this.__data__.push(value);
                this.size ++;
            }
            return this;
        }
        ,
        clear: function() {
            this.__data__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(value) {
            var ret = false,
                outIdx = {};
            if (this.has(value, outIdx)) {
                this.__data__.splice(outIdx.idx, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg) {
            var me = this;
            thisarg = thisarg || this;
            this.__data__.forEach(function(value) {
                callback(value, value, me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Set = Set;
})(Set, window || global);


/***
 * Array.from()
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
 * */
// Production steps of ECMA-262, Edition 6, 22.1.2.1
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method 
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

/***
 * String.endWith
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith#Polyfill
 * */
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.lastIndexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}

/***
 * String.startWith
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
 * */
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
}

/***
 * ParentNode/children
 * 
 * https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/children
// Overwrites native 'children' prototype.
// Adds Document & DocumentFragment support for IE9 & Safari.
// Returns array instead of HTMLCollection.
 * 
 * */
(function(ctor) {
    if (ctor &&
        ctor.prototype &&
        ctor.prototype.children == null) {
        Object.defineProperty(ctor.prototype, 'children', {
            get: function() {
                var i = 0, node, nodes = this.childNodes, children = [];
                while (node == nodes[i++]) {
                    if (node.nodeType === 1) {
                        children.push(node);
                    }
                }
                return children;
            }
        });
    }
})(window.Node || window.Element);
