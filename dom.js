export default $;

import { Bee } from "./bee.js";
import { fn } from "./fn.js";

function $(selector, context, firstonly) {
    return new DOM(selector, context, firstonly);
}

$.fn = fn;

class DOM {
    constructor(selector, context, firstonly) {
        if (!selector) {
            // TODO: return closure:
            //       1. when no selector or invalid selector, closure contains methods 
            //          which have nothing to do with elements (such as util), which is a singletone object
            //       2. when valid selector presents, closure return this instance, excluding the "_x" or "__x" objects
            //          eg. return {html: html.bind(this)}
            return fn;
        }
        
        let ret = this,
            contextElement;
        
        if (!!context) {
            contextElement = context;
        } else {
            contextElement = document;
        }
        
        this.length = 0;

        if (typeof selector == "string") {
            let match = null;
            if (null != (match = fn.parseHtml(selector))) {
                if (Array.isArray(match)) {
                    throw "Only a html snippet with a root element is supported.";
                }
                let ele = document.createElement(match["tag"]);
                
                match["attributes"].forEach(a => {
                    ele.setAttribute(a.attrName, a.attrValue);
                });
                
                ele.innerHTML = match["innerHTML"];
                this.push(ele);
            } else if (contextElement === document || this.isElement(contextElement)) {
                this.merge(this.query(contextElement, selector, firstonly));
            } else if (contextElement instanceof DOM) {
                contextElement.forEach((index, element) => {
                    this.merge(this.query(element, selector, firstonly));
                });
            }
        } else if (Array.isArray(selector)) {
            this.merge(selector);
        } else if (this.isElement(selector)) {
            this.push(selector);
        } else if (selector instanceof DOM) {
            ret = selector;
        }
        
        this.$$bee = new Bee(this);
        
        return ret;
    }
    
    on(eventtypes, handler, useCapture=false, thisarg=null) {
        this.$$bee.on(eventtypes, handler, useCapture, thisarg);
        return this;
    }
    
    off(eventtypes, handler) {
        this.$$bee.off(eventtypes, handler);
        return this;
    }
    
    attach() {
        this.$$bee.attach();
        return this;
    }
    
    detach() {
        this.$$bee.detach();
        return this;
    }
    
    eventProxy(eventtypes, func) {
        this.$$bee.proxy(eventtypes, func);
        return this;
    }
    
    push() {
        Array.prototype.push.apply(this, Array.from(arguments));
        return this;
    }
    
    slice() {
        Array.prototype.slice.apply(this, Array.from(arguments));
    }
    
    splice() {
        Array.prototype.splice.apply(this, Array.from(arguments));
    }
    
    each(callback) {
        if (callback)
        for(let i = 0; i < this.length; i++) {
            if (callback(i, this[i])) {
                break;
            }
        }
        return this;
    }
    
    html(value) {
        let ret = this;
        if (this.isHtml(value)) {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    element.innerHTML = value;
                }
            });
        } else {
            ret = "";
            this.each((index, element) => {
                if (this.isElement(element)) {
                    ret += element.innerHTML;
                }
            });
        }
        return ret;
    }
    
    ready(contendsonly, callback) {
        let _contendsonly = contendsonly,
            _callback = callback;
            
        if (undefined == callback) {
            _contendsonly = true;
            _callback = contendsonly;
        }
        
        if (_contendsonly) {
            document.addEventListener("DOMContentLoaded", callback);
        } else {
            window.addEventListener("load", callback);
        }
    }
    
    hasClass(className) {
        let ret = false,
            regClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
        
        this.each((index, element) => {
            if (this.isElement(element) && regClass.test(element.className)) {
                ret = true;     // hasClass = true
                return false;   // break from each
            }
        });
        
        return ret;
    }
    
    addClass(value) {
        if (typeof value === "function") {
            /* A function returning one or more space-separated class names to be added to the existing class name(s).
             * Within the function, this refers to the current element in the set
             */
            this.each((index, element) => {
                let rst = value.call(element, index, element.className);
                if (!!rst) {
                    fn.addClassToElement(element, rst);
                }
            });
        } else {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    fn.addClassToElement(element, value);
                }
            });
        }
        
        return this;
    }
    
    removeClass(value) {
        if (typeof value === "function") {
            /* A function returning one or more space-separated class names to be removed to the existing class name(s).
             * Within the function, this refers to the current element in the set
             */
            this.each((index, element) => {
                let rst = value.call(element, index, element);
                if (!!rst) {
                    element.className = fn.removeClassFromElement(element, rst);
                }
            });
        } else {
            this.each((index, element) => {
                if (this.isElement(element)) {
                    fn.removeClassFromElement(element, value);
                }
            });
        }
        
        return this;
    }
    
    /** Better performance than removeClass->addClass  */
    replaceClass(oldclass, newclass) {
        let classes;
        if (typeof oldclass === "string") {
            if (undefined != newclass) {
                classes = {};
                classes.oldclass = newclass;
            }
        } else if (typeof oldclass === "object") {
            classes = oldclass;
        }
        
        if (undefined == classes) {
            throw new Error("replaceClass(): Invalid paramter.");
        }
        
        this.each(function(index, element) {
            let cls = element.className;
            cls.split(" ").forEach(function(cur, idx, arr) {
                if (classes.hasOwnProperty(cur)) {
                    cls = cls.replace(new RegExp("(^|\\s)" + cur + "(\\s|$)"), " " + classes[cur] + " ");
                }
            });
            element.className = cls.replace(/ {2,}/g, " ");
        });
        
        return this;
    }
    
    attr (attrName, value) {
        if (undefined == value) {
            return this[0].getAttribute(attrName);
        } else {
            this.each(function(index, element) {
                if (this.isElement(element)) {
                    element.setAttribute(attrName, value);
                }
            }.bind(this));
        }
        
        return this;
    }
    
    hasAttribute(attr) {
        return this[0].hasAttribute(attr);
    }
    
    // TODO: (property [, value])
    css(prop, value) {
        let ret = this;
        if (undefined != value) {
            this.each(function(index, element) {
                element.style[prop] = value;
            });
        } else {
            if ("string" === typeof prop) {
                ret = this[0].style[prop];
            } else if (Array.isArray(prop)) {
                if (0 < prop.length) {
                    ret = {};
                    prop.forEach(function(p) {
                        ret[p] = this[0].style[p];
                    }.bind(this));
                }
            } else if ("object" === typeof prop ) {
                let cssText = "";
                for (let key in prop) {
                    if (prop.hasOwnProperty(key)) {
                        cssText += key + ":" + prop[key] + ";";
                    }
                }
                this.cssText(cssText);
            }
        }
        return ret;
    }
    
    cssText(csstext) {
        let ret = this[0].cssText;
        if (!!csstext && typeof(csstext) == "string") {
            let csst = csstext.trim();
            csst = csst.replace(/(\r\n|\n|\r)/gm,"");
            if (!csst.endsWith(";")) {
                csst += ";"; 
            }
            
            let pattern = /\s*([^;|^:]+):[^;]*;/g;
            let props = [];
            let match;
            while (null != (match = pattern.exec(csst))) {
                props.push(match[1]);
            }
            
            this.each(function(index, element) {
                let oriStyle = element.style.cssText;
                if (!oriStyle.endsWith(";")) {
                    oriStyle = oriStyle + ";";
                }
                
                props.forEach(function(prop) {
                   oriStyle = oriStyle.replace(new RegExp("\\s*" + prop + "\\s*:[^;]*;\\s*", "gi"), "");
                });
                
                let delim = oriStyle.endsWith(";") ? "" : ";";
                element.style.cssText = oriStyle + delim + csst;
            });
            
            ret = this;
        }
        return ret;
    }
    
    val(value) {
        return this.attr("value", value);
    }
    
    text(text) {
        let ret = this;
        
        if (undefined != text) {
            this.each(function(index, element) {
                element.innerHTML = text;
            });
        } else {
            ret = "";
            this.each(function(index, element) {
                ret += element.innerHTML + " ";
            });
        }
        
        return ret;
    }
    
    viewportSize() {
        let h = window.innerHeight 
              || document.documentElement.clientWidth 
              || document.getElementsByTagName("body")[0].clientWidth
              ,
            w = window.innerWidth
              || document.documentElement.clientHeight
              || document.getElementsByTagName("body")[0].clientHeight
              ;
        
        return {width: w, height: h};
    }
    
    // TODO: make getComputedStyle returned value a sington one.
    // in regard to the returned value of getComputedStyle, it reads
    // "returned style is a live CSSStyleDeclaration object, which updates itself automatically 
    //  when the element's style is changed."
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/getComputedStyle
    
    /**
     * Set or get the element width value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    width(w) {
        let ret = this;
        
        if (undefined != w) {
            if (typeof(w) === "number") {
                w += "px";
            }
            this.each(function(index, element) {
                element.style.width = w;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null).width);
        }
        
        return ret;
    }
    
    /**
     * Set or get the element width value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    height(h) {
        let ret = this;
        if (undefined != h) {
            if (typeof(h) === "number") {
                h += "px";
            }
            this.each(function(index, element) {
                element.style.height = h;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null).height);
        }
        
        return ret;
    }
    
    /**
     * Set or get the element top value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    top(t) {
        let ret = this;
        
        if (undefined != t) {
            if (typeof(t) === "number") {
                t += "px";
            }
            this.each(function(index, element) {
                element.style.top = t;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null)).top;
        }
        
        return ret;
    }
    
    /**
     * Set or get the element left value in px (but the "px" will not be presented in the returned value.)
     * If value with unit (such as px, vm, %) is desired, use css() method instead.
     **/
    left(l) {
        let ret = this;
        
        if (undefined != l) {
            if (typeof(l) === "number") {
                l += "px";
            }
            this.each(function(index, element) {
                element.style.left = l;
            });
        } else {
            ret = parseFloat(window.getComputedStyle(this[0], null)).left;
        }
        
        return ret;
    }
    
    show() {
        this.each(function(index, element){
            element.style.display = "";
        });
        
        return this;
    }
    
    hide() {
        this.each(function(index, element){
            element.style.display = "none";
        });
        
        return this;
    }
    
    visible(bvisible) {
        this.each(function(index, element){
            element.style.visibility = bvisible ? "visible" : "hidden";
        });
        
        return this;
    }
    
    onOrientationChanged(listener) {
        let mqlPortrait = !!window.matchMedia ? window.matchMedia("(orientation: portrait)") : null;
        if (!!mqlPortrait) {
            mqlPortrait.addListener((mql) => {
                listener(mqlPortrait.matches);
            });
        } else {
            window.addEventListener("resize", function() {
                listener(window.innerHeight > window.innerWidth); // windows.innerHeight / innerWidth would cause reflow
            });
        }
        return this;
    }
    
    append(content) {
        if (this.isElement(content)) {
            this.each((index, element) => {
                element.appendChild(content);
            });
        } else if (this.isHtml(content)) {
            this.append(new DOM(content)[0]);
        } else if (content instanceof DOM) {
            this.append(content[0]);
        }

        return this;
    }
    
    appendTo(owner) {
        if (undefined != owner) {
            this.each((index, element) => {
                new DOM(owner).append(element);
            });
        }
        
        return this;
    }
    
    remove(child) {
        if (this.isElement(child)) {
            this.each((index, element) => {
                element.removeChild(child);
            });
        } else if (child instanceof DOM) {
            this.each((index, element) => {
                child.forEach(function(index, child) {
                    element.removeChild(child);
                });
            });
        } else if (this.isString(child)) {
            this.remove(new DOM(child)).bind(this);
        } else if (null == child || undefined == child) {
            this.each((index, element) => {
                for (let i = 0; i < element.children.length; i++) {
                    element.removeChild(element.children[i]);
                }
            });
        }
        return this;
    }
}

fn.addClassToElement = function(element, classValue) {
    let cls = element.className.trim(),
        arr = fn.isArrayOrLike(classValue) 
            ? classValue
            : typeof classValue === "string"
            ? classValue.split(" ")
            : []
            ;
    
    for(let i=0; i<arr.length; i++) {
        let cur = arr[i];
        if (!cls) {
            cls = cur;
        } else if(0 > cls.split(" ").indexOf(cur)) { // case sensitive
            cls += " " + cur;
        }
        cls = cls.replace(/ {2,}/g, " ");
    }
    
    element.className = cls;
    
    return this;
};

fn.removeClassFromElement = function(element, classValue) {
    let cls = element.className,
        arr = fn.isArrayOrLike(classValue) 
            ? classValue
            : typeof classValue === "string"
            ? classValue.split(" ")
            : []
            ;
    
    if (!!cls) {
        for(let i=0; i<arr.length; i++) {
            cls = cls.replace(new RegExp("(^|\\s)" + arr[i] + "(\\s|$)", "g") ," ");
        }
        cls.replace(/ {2,}/g, " ");
    }
    element.className = cls;
    
    return this;
};

/**
 * description  parse "tag" "attributes" "innerHtml" from html (like) string.
 * 
 * TODO: the behavor should be like as follows. At the being, only one root element html snippet is supported.
 * If the string has only one "root" element, the return value is a object has tag/attributes/innerHtml key-value pair.
 * If the string has multi cousin element without "root" element, then the return value is an array contains key-value paires.
 *
 */
fn.parseHtml = function(html) {
    // TODO: /\s*((?:regex1)|(?:regex2))\s*/gm, (no ^ $), and parse to multiple cousin elements
    let regex1 = /^\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>]*)\s*>(.*)<\/([A-Za-z][A-Za-z0-9]+)\s*>\s*$/,
        regex2 = /^\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>\/]*)\s*\/{0,1}>\s*$/,
        result = null,
        match = null;
        
    if (typeof html == "string") {
        let arrAttr =[];
        if (null != (match = regex1.exec(html))) {
            if (match[1] === match[4]) {
                arrAttr = this.parseAttrStr(match[2]);
                result = {
                    tag: match[1],
                    attributes: arrAttr,
                    innerHTML: match[3]
                };
            } else {
                throw "The open tag does not match close tag";
            }
        } else if (null != (match = regex2.exec(html))) {
            arrAttr = this.parseAttrStr(match[2]);
            result = {
                tag: match[1],
                attributes: arrAttr,
                innerHTML: ""
            };
        }
    }
    
    return result;
};

fn.parseAttrStr = function(attrStr) {
    let arrAttr = [];
    if (!!attrStr && !/^\s+$/.test(attrStr)) {
        let a = attrStr.match(/(?:[^\s"']+|["'][^"']*["'])+/g) ;
        a.forEach(function(s) {
            if (!/^\s+$/.test(s)) {
                let kv = s.split("=");
                if (2 < kv.length) {
                    throw "Invalid html attribute format!";
                }
                let key = kv[0].trim();
                let val = null;
                if (2 == kv.length) {
                    val = kv[1].trim().replace(/^["']/, '').replace(/["']$/, '');
                }
                arrAttr.push({attrName: key, attrValue: val});
            }
        });
    }
    return arrAttr;
};

fn.query = function(element, selector, firstonly) {
    selector = selector.trim();
    let rst = [];
    
    if (firstonly) {
        rst.push(element.querySelector(selector));
    } else {
        rst = element.querySelectorAll(selector);
    }
    
    return rst;
};

/***
 * $.apply is different from $.fn.apply.
 * $.apply will apply the object to DOM.prototype,
 * while $.fn.apply can not apply to DOM instance or other specified object.
 * */
$.apply = function(obj) {
    fn.extend(true, DOM.prototype, obj);
    return $;
};

/** The line below should alway be at the bottom of this file. */
fn.extend(false, DOM.prototype, fn);
