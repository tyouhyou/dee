// export { fn };

/* global Element, HTMLElement*/

var fn = {
    isObject : function(obj) {
        return obj !== null && typeof obj === "object";
    }
    ,
    isCallable : function (fn) {
      return typeof fn === 'function' || Object.prototype.toString.call(fn) === '[object Function]';
    }
    ,
    isElement : function(obj) {
        return !obj
                ? false
                : (Element && typeof Element === "object")
                ? obj instanceof Element
                : (HTMLElement && typeof HTMLElement === "object")
                ? obj instanceof HTMLElement
                : (typeof obj === "object" && obj.nodeType === 1 && typeof obj.nodeName === "string")
                ;
    }
    ,
    /* global Node */
    isNode: function(obj) {
        return typeof Node === "object"
                ? obj instanceof Node
                : obj && typeof obj === "object" && typeof obj.nodeType === "number" && typeof obj.nodeName === "string"
                ;
    }
    ,
    isHtml: function(value) {
        var regexx = /^\s*<.*>\s*$/,
            regex1 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>]*)\s*>(.*)<\/([A-Za-z][A-Za-z0-9]+)\s*>\s*/g,
            regex2 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>\/]*)\s*\/{0,1}>\s*/g
            ;
        return regexx.test(value) && (regex1.test(value) | regex2.test(value));
    }
    ,
    isString : function(s) {
        return typeof s === "string" || s instanceof String;
    }
    ,
    isArrayOrLike : function(a) {
        return !a
                ? false
                : a instanceof Array
                ? true
                : typeof(a) == "object" && a.hasOwnProperty("length") && a.length >= 0 && a.length < Number.MAX_SAFE_INTEGER
                ;
    }
    ,
    /**
     * @description The objects which can apply forEach() to, but not enumerable (which means an object here.)
     *              Check if the target object can be used in for() loop.
     */
    isIterable : function(obj) {
        var rst = false,
            strType = Object.prototype.toString.call( obj );
        
        // TODO: add set, map, iterator?
        if (fn.isArrayOrLike(obj)
        ||  /^\[object (HTMLCollection|NodeList)\]$/.test(strType))
        {
            rst = true;
        }
        
        return rst;
    }
    ,
    isUndefine : function(obj) {
        return typeof obj === 'undefined';
    }
    ,
    isDate : function(dat) {
        return Object.prototype.toString.call(dat) === '[object Date]';
    }
    ,
    isRegexp : function(reg) {
        return Object.prototype.toString.call(reg) === '[object RegExp]';
    }
    ,
    /***
     * Parse words in a string delimited by space into word arrays.
     * */
    stringToArray : function(s) {
        var arr = s;
        if (fn.isString(s)) {
            arr = s.split(" ");
            arr = arr.filter(a => {
                    a = a.trim();
                    return !!a;
                });
        }
        return arr;
    }
    ,
    flattenArrayOrLike : function(arr) {
        var a = [];
        if (fn.isArrayOrLike(arr))
        for (var i = 0; i < arr.length; i++) {
            if (fn.isArrayOrLike(arr[i])) {
                a = a.concat(a, arr[i]);
            } else {
                a.push(arr[i]);
            }
        }
        return a;
    }
    ,
    flattenElements : function(elements, arr) {
        if (!window || !window.document) {
            throw new Error("Illegal operation. The context seems not a browser window object.");
        }

        if (arr && !fn.isArrayOrLike(arr)) {
            throw new Error("Invalid argment. The second argument should be an array.");
        }
        
        var elelist = arr || [];

        if (fn.isString(elements)) {
            elelist = window.document.body.querySelectorAll(elements);
        } else if (fn.isArrayOrLike(elements)) {
            fn.flattenArrayOrLike(elements).forEach( el => {
                if (fn.isString(el)) {
                    elelist = elelist.concat(elelist, window.document.body.querySelectorAll(el));
                } else if (fn.isElement(el)) {
                    elelist.push(el);
                } else {
                    throw new Error("Invalid argument. Selectors or HtmlElement objects are expected.");
                }
            });
        } else {
            throw new Error("Invalid argument. A string or an array (or -like) is expected");
        }

        return elelist;
    }
    ,
    /**
     * NOTE: the arguments other than plain object would be wrapped into an object.
     */
    shallowClone : function(obj) {
        var rst = obj;
        if (null === obj || undefined === obj) {
            rst = this;
        }
        if (typeof obj === "function") {
            rst = obj;
        } else if (obj !== null && obj !== undefined && obj instanceof Object) {
            for (var nextKey in obj) {
                if (obj.hasOwnProperty(nextKey)) {
                    rst[nextKey] = obj[nextKey];
                }
            }
        }
        return rst;
    }
    ,
    /**
     * description
     * refer to [https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm]
     */
    deepClone : function(obj) {
        var rst = obj,
            Constructor;

        if (null === obj && undefined === obj) {
            obj = this;
        }
        
        if (obj instanceof Object) {
            var copy = true;
            Constructor = obj.constructor;
            switch (Constructor) {
                /* global CanvasRenderingContext2D */
                case CanvasRenderingContext2D:
                    copy = false;   // for those special objects, do not need copy them
                case Function:
                    rst = obj;
                    break;
                case RegExp:
                case String:
                case Number:
                    rst = new Constructor(obj);
                    break;
                case Date:
                    rst = new Constructor(obj.getTime());
                    break;
                default:
                    rst = new Constructor();
                    break;
            }
            
            if (copy)
            for (var prop in obj) {
                rst[prop] = fn.deepClone(obj[prop]);
            }
        }
        
        return rst;
    }
    ,
    /**
     * @description perform shallow or deep cloning.
     * @param {boolean} [ifDeep=false]
     * @param {object} obj
     */
    clone : function(deep, obj) {
        var idx = 0,
            _deep = false,
            _obj;
        
        if (typeof arguments[idx] === "boolean") {
            _deep = arguments[idx++];
        }
        
        _obj = arguments[idx];
        if (undefined === _obj || null === obj) {
            _obj = this;
        }
        
        if (!_deep) {
            return fn.shallowClone(_obj);
        } else {
            return fn.deepClone(_obj);
        }
    }
    ,
    /**
     * 
     */
    extend : function(deep, target, obj) {
        var idx = 0,
            _deep = false,
            _target = this,
            _obj = obj;

        if (arguments.length <= 0) {
            return _target;
        }
        
        // deep
        if (typeof arguments[idx] === "boolean") {
            _deep = arguments[idx++];
        }
        
        // target
        if (idx >= arguments.length || !arguments[idx]) {
            return _target;
        }
        
        // set target
        _target = arguments[idx++];
        
        _obj = arguments[idx];
        
        // extends all objects to targets.
        for (; idx < arguments.length; idx++) {
            var o = _obj;
            if (o == null || o == undefined) continue;

            for (var prop in o) {
                _target[prop] = fn.clone(_deep, o[prop]);
            }
        }

        return _target;
    }
    ,
    apply : function(target, obj) {
        var _target = target,
            _obj = obj;
            
        if (1 == arguments.length) {
            _target = this;
            _obj = target;
        }
        fn.extend(true, _target, _obj);     // should not be affect by other objects who keep the obj.
        return this;
    }
    ,
    /**
     * Create a new function which inherits from parent, and with new properties against parent.
     */
    create : function(parent, newProperties, independant) {
        if (undefined == parent) return null;
        if (!newProperties) return parent;
        
        return function() {
            function T(){}
            if (parent.prototype) {
                T.prototype = fn.clone(independant, parent.prototype);
            }
            if (T.prototype) {
                fn.extend(independant, T.prototype, newProperties);
            }
            var t = new T();
            var p = parent.prototype;
            parent.prototype = t;
            var f = parent.apply(t, arguments);
            parent.prototype = p;
            return f;
        };
    }
    ,
    /** arguments: ([arraylikeobj], callback) */
    forEach : function(obj, callback, isToBreak) {
        var _obj = this,
            _callback,
            _breakSwitch = false;
        
        switch(arguments.length) {
            case 1:
                _callback = arguments[0];
                break;
            case 2:
                if (typeof arguments[0] === "function")
                {
                    _callback = arguments[0];
                    _breakSwitch = arguments[1];
                } else {
                    _obj = arguments[0];
                    _callback = arguments[1];
                }
                break;
            default:
                _obj = arguments[0];
                _callback = arguments[1];
                _breakSwitch = isToBreak;
                break;
        }
        
        if (typeof _obj === "object")
        if( fn.isIterable(_obj) ) {
            for (var i = 0; i < _obj.length; i++) {
                if (_callback.call(_obj[i], i, _obj[i]) === _breakSwitch) {
                    break;
                }
            }
        } else {
            for (var prop in _obj) {
                if (_callback.call(_obj[prop], prop, _obj[prop]) === _breakSwitch) {
                    break;
                }
            }
        }
        
        return obj;
    }
    ,
    /**
     * @description Merge two arrays.
     * TODO: distinct?
     */
    merge : function(arr1, arr2) {
        var
        _arr1 = arr1,
        _arr2 = arr2,
        _start;
        
        if (2 > arguments.length) {
            _arr1 = this;
            _arr2 = arr1;
        }
        
        _start = _arr1.length;

        for(var i = 0; i < _arr2.length; i++) {
            _arr1[i + _start] = _arr2[i];
        }
        
        _arr1.length = _arr1.length + _arr2.length;
        
        return _arr1;
    }
    ,
    format : function(fmt) {
        if (!fmt) {
            return fmt;
        }
        
        var idx,
            match,
            reg = /({\s*(\d+)\s*})/g;
        
        while (null != (match = reg.exec(fmt))) {
            if (!isNaN(idx = Number.parseInt(match[2], 10))) {
                fmt = fmt.replace(match[1], arguments[idx+1]);      // {x} x is 0 based
            } else {
                break;
            }
        }
        return fmt;
    }
    ,
    equals : function(o1, o2) {
        var ret = (o1 === o2);
        if (ret) return true;
        
        if (typeof(o1) === "number" && isNaN(o1) && 
            typeof(o2) === "number" && isNaN(o2)) {
                return true;
            }
            
        if (!o1 && !o2) {
            if (Array.isArray(o1) && Array.isArray(o2) && o1.length == o2.length) {
                ret = true;
                for (var i = 0; i < o1.length; i++) {
                    if (!this.equals(o1[i], o2[i])) {
                        ret = false;
                        break;
                    }
                }
            } else if (this.isObject(o1) && this.isObject(o2)) {
                ret = true;
                for(var p1 in o1) {
                    if (o1.hasOwnProperty(p1) && !this.equals(o1[p1], o2[p1])) {
                        ret = false;
                        break;
                    }
                }
                if (ret) {
                    for(var p2 in o2) {
                        if (o2.hasOwnProperty(p2) && !this.equals(o1[p2], o2[p2])) {
                            ret = false;
                            break;
                        }
                    }
                }
            }
        }
        
        return ret;
    }
};

/** Add methods to String object */
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = [].slice.call(arguments);
        args.unshift(this);
        return fn.format.apply(this, args);
    };
}
