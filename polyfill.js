
/***
 * Polyfill for es6 Map object. Not all methods will be implemented.
 * Just those methods we will use.
 * 
 * Since this polyfill is just for using with es5, iterator would not be honored.
 * */
(function(context) {
    if (!!context.Map) return;
    
    var Map = function(array) {
        if (!(this instanceof Map)) {
            return new Map(array);
        }
        
        var check = true;
        if (!Array.isArray()) {
            check = false;
        }
        array.forEach(function(keyvalue){
            if (!Array.isArray(keyvalue) || keyvalue.length != 2) {
                check = false;
                return;
            }
        }, this);
        if (!check) {
            throw new Error("The argument sent to Map constructor should be a key-value pair array.");
        }
        
        this.__keyvaluepairs__ = array || new Array();
        this.size = 0;
    };
    
    Map.prototype = {
        _idxKey: 0,
        _idxValue: 1,
        /***
         * check if the specified key has been set. 
         * out_obj_for_idx is not presented in the standard IF.
         * It is here to keep the key location, and just for using within the Map object.
         * */
        has: function(key, out_obj_for_idx) {
            var ret = false;
            // since Nan cannot be handled correctly by indexOf(), we iterate via forEach.
            this.__keyvaluepairs__.forEach(function(keyvalue, index){
                if ((typeof(key) === "number" && isNaN(key) && 
                    typeof(keyvalue[this._idxKey]) === "number" && isNaN(keyvalue[this._idxKey]))  ||
                    key === keyvalue[0]) 
                {
                    ret = true;
                    if (!!out_obj_for_idx && (out_obj_for_idx instanceof Object)) {
                        out_obj_for_idx.index = index;
                    }
                    return;
                }
            }, this);
            return ret;
        }
        ,
        set: function(key, value) {
            if (this.has(key)) {
                this.__keyvaluepairs__[key][this._idxValue] = value;
            } else {
                this.__keyvaluepairs__.push([key, value]);
                this.size ++;
            }
            return this;
        }
        ,
        get: function(key) {
            var ret;
            if (this.has(key)) {
                ret = this.__keyvaluepairs__[key];
            }
            return ret;
        }
        ,
        clear: function() {
            this.__keyvaluepairs__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(key) {
            var ret = false,
                idx = {};
            if (this.has(key, idx)) {
                this.__keyvaluepairs__.splice(idx.index, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg){
            var me = this;
            thisarg = thisarg || this;
            this.__keyvaluepairs__.forEach(function(keyvalue) {
                callback(keyvalue[me._idxValue], keyvalue[me._idxKey], me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Map = Map;
})(window || global);

(function(context){
    if (!!context.Set) return;
    
    var Set = function(array) {
        if (!(this instanceof Set)) {
            return new Set(array);
        }
        
        this.size = 0;
        this.__data__ = [];
    };
    
    Set.prototype = {
        has: function(value, out_obj_for_idx) {
            var ret = false;
            if (typeof(value) === "number" && isNaN(value)) {
                this.__data__.forEach(function(val, index) {
                    if (typeof(val) === "number" && isNaN(val)) {
                        ret = true;
                        out_obj_for_idx.idx = index;
                    }
                });
            } else if ((out_obj_for_idx.idx = this.__data__.indexOf(value, 0)) >= 0) {
                ret = true;
            }
            return ret;
        }
        ,
        add: function(value) {
            if (!this.has(value)) {
                this.__data__.push(value);
                this.size ++;
            }
            return this;
        }
        ,
        clear: function() {
            this.__data__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(value) {
            var ret = false,
                outIdx = {};
            if (this.has(value, outIdx)) {
                this.__data__.splice(outIdx.idx, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg) {
            var me = this;
            thisarg = thisarg || this;
            this.__data__.forEach(function(value) {
                callback(value, value, me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Set = Set;
})(Set, window || global);


/***
 * Array.from()
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
 * */
// Production steps of ECMA-262, Edition 6, 22.1.2.1
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method 
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

/***
 * String.endWith
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith#Polyfill
 * */
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.lastIndexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}

/***
 * String.startWith
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
 * */
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
}

/***
 * ParentNode/children
 * 
 * https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/children
// Overwrites native 'children' prototype.
// Adds Document & DocumentFragment support for IE9 & Safari.
// Returns array instead of HTMLCollection.
 * 
 * */
(function(ctor) {
    if (ctor &&
        ctor.prototype &&
        ctor.prototype.children == null) {
        Object.defineProperty(ctor.prototype, 'children', {
            get: function() {
                var i = 0, node, nodes = this.childNodes, children = [];
                while (node == nodes[i++]) {
                    if (node.nodeType === 1) {
                        children.push(node);
                    }
                }
                return children;
            }
        });
    }
})(window.Node || window.Element);
